This module provides integration for MailChimp module with Panels. There is no
special configuration for this module.

Using MailChimp Signup Form with Panes
--------------------------------------------------------------------------------
  * Once you create or update MailChimp signup forms at "admin/config/services/mailchimp/signup",
    clear cache to allow site to discover objects associated with signup form.

  * Go to Panels Page Manager page or Panels Mini Panel where you want to add the MailChimp
    signup form.

  * Add context for the signup form. It will be available in context list with
    label like "MailChimp Signup Form (xxx)", where "xxx" will be your signup
    form name.

  * Go to "Content" part of your Panels Page Manager page or Panels Mini Panel. The field
    associated with the signup form will be available in "MailChimp Signup Form: xxx"
    vertical tab of "Add content..." popup.

  * The submit actions and description panes will be available in "MailChimp Signup Form"
    vertical tab of "Add content..." popup.

  * You have to add "General form" pane available in "Form" vertical tab of "Add content..."
    popup to ensure all fields that you did not add to separate panes are also
    being displayed.  This is optional, but will make sure the form continues to work if you
    add new fields.
