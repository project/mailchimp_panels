<?php

/**
 * @file
 * Plugin to provide a user_edit_form context.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('MailChimp Signup Form'),
  'description' => t('A MailChimp Signup Form.'),
  'context' => 'mailchimp_panels_context_create_mailchimp_signup_form',
  'edit form' => 'mailchimp_panels_context_mailchimp_signup_form_settings_form',
  'defaults' => array(),
  'convert list' => 'mailchimp_panels_context_mailchimp_signup_form_convert_list',
  'convert' => 'mailchimp_panels_context_mailchimp_signup_form_convert',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the user ID of a user for this argument:'),
  ),
  // Tell ctools_context_get_context_from_context() how to interpret its $argument.
  'placeholder name' => 'signup_identifier',
  'get child' => 'mailchimp_panels_context_mailchimp_signup_form_get_child',
  'get children' => 'mailchimp_panels_context_mailchimp_signup_form_get_children',
);

function mailchimp_panels_context_mailchimp_signup_form_get_child($plugin, $parent, $child) {
  $plugins = mailchimp_panels_context_mailchimp_signup_form_get_children($plugin, $parent);
  return $plugins[$parent . ':' . $child];
}

function mailchimp_panels_context_mailchimp_signup_form_get_children($plugin, $parent) {

  $signups = mailchimp_signup_load_multiple();

  $plugins = array();
  foreach ($signups as $signup) {
    $child_plugin = $plugin;
    $child_plugin['title'] = t('MailChimp Signup Form (@signup_label)', array('@signup_label' => $signup->label()));
    $child_plugin['keyword'] = $signup->identifier();
    $child_plugin['context name'] = $signup->identifier();
    $child_plugin['name'] = $parent . ':' . $signup->identifier();
    $child_plugin['description'] = t('Creates @entity context from an entity ID.', array('@entity' => $signup->identifier()));
    $child_plugin_id = $parent . ':' . $signup->identifier();
    drupal_alter('mailchimp_panels_mailchimp_signup_form_context', $child_plugin, $signup, $child_plugin_id);
    $plugins[$child_plugin_id] = $child_plugin;
  }
  drupal_alter('mailchimp_panels_mailchimp_signup_form_contexts', $plugins);
  return $plugins;
}

/**
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function mailchimp_panels_context_create_mailchimp_signup_form($empty, $user = NULL, $conf = FALSE, $plugin) {
  $signup_identifier = $plugin['keyword'];

  $context = new ctools_context(array('mailchimp_signup_form:' . $signup_identifier, 'mailchimp_signup_form', 'form', $signup_identifier));
  // Store this context for later.

  $context->plugin = $plugin['name'];
  $context->keyword = $signup_identifier;
  if ($empty) {
    return $context;
  }

  if (empty($context->data)) {
    $data = mailchimp_signup_load($signup_identifier);
  }

  if (!empty($data)) {
    $context->data = $data;
    $context->title = t('MailChimp Signup Form (@signup_label)', array('@signup_label' => $data->label()));

    $form_id = 'mailchimp_signup_subscribe_form';
    // Form mailchimp_signup_subscribe_form() does not appear to be using $type parameter,
    // so not sure about passing 'mailchimp_signup_subscribe_form'
    $form_state = array('want form' => TRUE, 'build_info' => array('args' => array($data, 'mailchimp_signup_block')));
    $form = drupal_build_form($form_id, $form_state);

    $context->form       = $form;
    $context->form_state = &$form_state;
    $context->form_id    = $form_id;

    $context->restrictions['form'] = array('form');
    return $context;
  }
}

function mailchimp_panels_context_mailchimp_signup_form_settings_form($form, &$form_state) {
  $conf = &$form_state['conf'];

  return $form;
}

/**
 * Validate a node.
 */
function mailchimp_panels_context_mailchimp_signup_form_settings_form_validate($form, &$form_state) {
}

function mailchimp_panels_context_mailchimp_signup_form_settings_form_submit($form, &$form_state) {

  $form_state['conf']['signup_form'] = $form_state['values']['signup_form'];
}
/**
 * Provide a list of ways that this context can be converted to a string.
 */
function mailchimp_panels_context_mailchimp_signup_form_convert_list($plugin) {
  $list = array();

  return $list;
}

/**
 * Convert a context into a string.
 */
function mailchimp_panels_context_mailchimp_signup_form_convert($context, $type, $options = array()) {
}
