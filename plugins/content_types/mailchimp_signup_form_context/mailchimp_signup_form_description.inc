<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'icon' => 'icon_node_form.png',
  'title' => t('MailChimp Signup Form Description'),
  'description' => t('MailChimp Signup Form Description.'),
  'required context' => new ctools_context_required(t('MailChimp Signup Form'), 'mailchimp_signup_form'),
  'category' => t('MailChimp Signup Form'),
);

function mailchimp_panels_mailchimp_signup_form_description_content_type_render($subtype, $conf, $panel_args, &$context) {
  $block = new stdClass();
  $block->module = t('mailchimp_panels');
  
  $block->title = '';
  $block->delta = 'description';
  
  if (isset($context->form)) {
    $block->content = array();
    $block->content['description'] = isset($context->form['description']) ? $context->form['description'] : NULL;
    unset($context->form['description']);
  }
  else {
    $block->content = t('MailChimp Signup Form Description.');
  }
  return $block;
}

function mailchimp_panels_mailchimp_signup_form_description_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" Description', array('@s' => $context->identifier));
}

function mailchimp_panels_mailchimp_signup_form_description_content_type_edit_form($form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  return $form;
}
