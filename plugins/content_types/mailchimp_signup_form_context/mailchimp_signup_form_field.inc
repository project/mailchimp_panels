<?php

/**
 * @file
 * Handle rendering MailChimp Signup Form fields as panes.
 */

$plugin = array(
  'title' => t('MailChimp Signup Form Field'),
  'defaults' => array('label' => 'before'),
  'content type' => 'mailchimp_panels_mailchimp_signup_form_field_content_type_content_type',
  'admin title' => 'ctools_mailchimp_signup_form_field_content_type_admin_title',
);

/**
 * Just one subtype.
 *
 * Ordinarily this function is meant to get just one subtype. However, we are
 * using it to deal with the fact that we have changed the subtype names. This
 * lets us translate the name properly.
 */
function mailchimp_panels_mailchimp_signup_form_field_content_type_content_type($subtype) {
  $types = mailchimp_panels_mailchimp_signup_form_field_content_type_content_types();
  if (isset($types[$subtype])) {
    return $types[$subtype];
  }
}

/**
 * Return all field content types available.
 */
function mailchimp_panels_mailchimp_signup_form_field_content_type_content_types() {
  $types = &drupal_static(__FUNCTION__, array());
  if (!empty($types)) {
    return $types;
  }

  $cache_key = 'mailchimp_panels_mailchimp_signup_form_field_content_type_content_types';
  if ($cache = cache_get($cache_key)) {
    $types = $cache->data;
    if (!empty($types)) {
      return $types;
    }
  }

  // This will hold all the individual field content types.
  $context_types = array();
  $signups = mailchimp_signup_load_multiple();

  $description = t('Field on the referenced MailChimp Signup Form.');
  $categories = array();
  foreach ($signups as $signup) {
    $category = t('MailChimp Signup Form: @signup', array('@signup' => $signup->label()));
    $categories[$signup->identifier()] = $category;
    foreach ($signup->settings['mergefields'] as $tag => $mergevar) {
      if (empty($mergevar)) {
        continue;
      }
      $label = $mergevar->name;
      $types[$signup->identifier() . ':' . $tag] = array(
        'category' => $category,
        'icon' => 'icon_field.png',
        'title' => t('Field: @widget_label (@field_name)', array(
          '@widget_label' => $label,
          '@field_name' => $tag,
        )),
        'description' => $description,
        'edit form' => array(
          'ctools_mailchimp_signup_form_field_content_type_formatter_options' => array(
            'default' => TRUE,
            'title' => t('Formatter options for: @widget_label (@field_name)', array(
              '@widget_label' => $label,
              '@field_name' => $tag,
            )),
          ),
        ),
      );
      $context_types[$signup->identifier() . ':' . $tag]['types'][$signup->identifier()] = $signup->label();
    }
  }

  // Create the required context for each field related to the bundle types.
  foreach ($types as $key => $field_content_type) {
    list($signup_identifier, $field_name) = explode(':', $key, 2);
    $types[$key]['required context'] = new ctools_context_required($categories[$signup_identifier], $signup_identifier, array(
      'type' => array_keys($context_types[$key]['types']),
    ));
    unset($context_types[$key]['types']);
  }

  cache_set($cache_key, $types);

  return $types;
}

/**
 * Render the custom content type.
 */
function mailchimp_panels_mailchimp_signup_form_field_content_type_render($subtype, $conf, $panel_args, $context) {
  if (empty($context) || empty($context->data)) {
    return;
  }

  // Get a shortcut to the entity.
  list(, $field_name) = explode(':', $subtype, 2);

  // Do not render if the entity type does not have this field or group.
  if (empty($context->form['mergevars'][$field_name])) {
    return;
  }

  $block = new stdClass();
  if (isset($context->form)) {
    $block->content = array();
    if (!empty($context->form['mergevars'][$field_name])) {
      $block->content[$field_name] = $context->form['mergevars'][$field_name];
      unset($context->form['mergevars'][$field_name]);
    }
    else {
      // Pre-render the form to populate field groups.
      if (isset($context->form['#pre_render'])) {
        foreach ($context->form['#pre_render'] as $function) {
          if (function_exists($function)) {
            $context->form = $function($context->form);
          }
        }
        unset($context->form['#pre_render']);
      }

      $block->content[$field_name] = $context->form['mergevars'][$field_name];
      unset($context->form['mergevars'][$field_name]);
    }
  }
  else {
    $block->content = t('@field field', array('@field' => $field_name));
  }

  if (isset($block->content[$field_name]['#title'])) {
    if ($conf['label'] == 'title') {
      $block->title = $block->content[$field_name]['#title'];
      $block->content[$field_name]['#title_display'] = 'invisible';
    }
    else{
      $block->content[$field_name]['#title_display'] = $conf['label'];
    }
  }

  return $block;
}

/**
 * Replace context keywords.
 */
function ctools_mailchimp_signup_form_field_content_type_substitute_keywords($markup, array $element) {
  return ctools_context_keyword_substitute($markup, array(), array($element['#ctools_context']));
}

/**
 * Returns an edit form for custom type settings.
 */
function ctools_mailchimp_signup_form_field_content_type_formatter_options($form, &$form_state) {
  $conf = $form_state['conf'];

  $field_label_options = array(
    'title' => t('Pane title'),
    'before' => t('Before'),
    'after' => t('After'),
    'invisible' => t('Invisible'),
  );

  $form['label'] = array(
    '#type' => 'select',
    '#title' => t('Label Display'),
    '#options' => $field_label_options,
    '#default_value' => $conf['label'],
  );

  return $form;
}

function ctools_mailchimp_signup_form_field_content_type_formatter_options_submit($form, &$form_state) {
  $form_state['conf']['formatter'] = $form_state['values']['formatter'];
  $form_state['conf']['label'] = $form_state['values']['label'];
}

/**
 * Returns the administrative title for a type.
 */
function ctools_mailchimp_signup_form_field_content_type_admin_title($subtype, $conf, $context) {
  list(, $field_name) = explode(':', $subtype);
  if (is_object($context) && isset($context->identifier)) {
    $identifier = $context->identifier;
  }
  else {
    watchdog('ctools_mailchimp_signup_form_field_content_type_admin_title', 'Context is missing for field: @name', array('@name' => $subtype), WATCHDOG_NOTICE);
    $identifier = t('Unknown');
  }

  if (isset($context->data->settings['mergefields'][$field_name]->name)) {
    $field = $context->data->settings['mergefields'][$field_name]->name;
  }
  else {
    $field = $field_name;
  }

  return t('"@s" @field field', array('@s' => $identifier, '@field' => $field));
}
