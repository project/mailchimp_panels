<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'icon' => 'icon_node_form.png',
  'title' => t('MailChimp Signup Form Interest Groups'),
  'description' => t('MailChimp Signup Form Interest Groups.'),
  'required context' => new ctools_context_required(t('MailChimp Signup Form'), 'mailchimp_signup_form'),
  'category' => t('MailChimp Signup Form'),
);

function mailchimp_panels_mailchimp_signup_form_interest_groups_content_type_render($subtype, $conf, $panel_args, &$context) {
  $block = new stdClass();
  $block->module = t('mailchimp_panels');

  $block->title = '';
  $block->delta = 'interest_groups';

  if (isset($context->form)) {
    $block->content = array();
    $block->content['interest_groups'] = isset($context->form['mailchimp_lists']['interest_groups']) ? $context->form['mailchimp_lists']['interest_groups'] : NULL;
    unset($context->form['mailchimp_lists']['interest_groups']);
  }
  else {
    $block->content = t('MailChimp Signup Form Interest Groups.');
  }
  return $block;
}

function mailchimp_panels_mailchimp_signup_form_interest_groups_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" Interest Groups', array('@s' => $context->identifier));
}

function mailchimp_panels_mailchimp_signup_form_interest_groups_content_type_edit_form($form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  return $form;
}
