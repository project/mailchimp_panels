<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'icon' => 'icon_node_form.png',
  'title' => t('MailChimp Signup Form Actions'),
  'description' => t('MailChimp Signup Form Actions includng submit buttons.'),
  'required context' => new ctools_context_required(t('MailChimp Signup Form'), 'mailchimp_signup_form'),
  'category' => t('MailChimp Signup Form'),
);

function mailchimp_panels_mailchimp_signup_form_actions_content_type_render($subtype, $conf, $panel_args, &$context) {
  $block = new stdClass();
  $block->module = t('mailchimp_panels');
  
  $block->title = '';
  $block->delta = 'actions';
  
  if (isset($context->form)) {
    $block->content = array();
    foreach (array('actions', 'form_token', 'form_build_id', 'form_id') as $element) {
      $block->content[$element] = isset($context->form[$element]) ? $context->form[$element] : NULL;
      unset($context->form[$element]);
    }
  }
  else {
    $block->content = t('MailChimp Signup Form Actions.');
  }
  return $block;
}

function mailchimp_panels_mailchimp_signup_form_actions_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" Actions', array('@s' => $context->identifier));
}

function mailchimp_panels_mailchimp_signup_form_actions_content_type_edit_form($form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  return $form;
}
